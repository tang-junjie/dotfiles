My Emacs and other configs for GNU/Linux
========================================

**DEPRECATION NOTICE 2020-05-04.** I am no longer using BSPWM and
several of its accoutrements.  My focus is on my comprehensive Emacs
setup and whatever external tweak may be necessary to improve the
experience with that tool.

The last state of my dotfiles with BSPWM and its extras can be found in
[tag v2.2.0](https://gitlab.com/protesilaos/dotfiles/-/tree/v2.2.0).  I
provide no change logs and no support whatsover (see the README of that
tag for more information).

Do not track my dotfiles
------------------------

My dotfiles' repo functions as a laboratory for my computing
environment.  I offer no support whatsoever about tracking it and may
introduce breaking changes without prior notice (though my dotemacs is
thoroughly documented and I do 100% support all my other projects).

That granted, you can always open an issue here or contribute any fixes.

Copying
-------

Unless otherwise noted, all code herein is distributed under the terms
of the GNU General Public License Version 3 or later.
